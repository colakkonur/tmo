﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tmo.Models.Login
{
    public class LoginState
    {
        public LoginState()
        {
            tmoEntities db = new tmoEntities();
        }
        public bool IsLoginSucces(string user, string pass)
        {
            tmoEntities db = new tmoEntities();
            kullanicilar resultUser = db.kullanicilar.Where(x => x.kullanici_adi.Trim().ToString().Equals(user) && x.parola.Equals(pass)).FirstOrDefault();
            if (resultUser != null)
            {
                HttpContext.Current.Session.Add("kullanici_id", resultUser.kullanici_id.ToString());
                HttpContext.Current.Session.Add("kullanici_adi", resultUser.kullanici_adi.ToString());
                HttpContext.Current.Session.Add("ad_soyad", resultUser.ad_soyad.ToString());
                HttpContext.Current.Session.Add("rol", resultUser.rol.ToString());
                return true;
            }
            return new bool();

        }
    }
}