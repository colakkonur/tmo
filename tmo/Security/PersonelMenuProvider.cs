﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using tmo.Models;

namespace tmo.Security
{
    public class PersonelMenuProvider : RoleProvider
    {
        public override string ApplicationName
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string kullaniciId)
        {
            tmoEntities db = new tmoEntities();
            var vKullanici = db.kullanici_alt_menu.Where(w => w.kullanici_id.ToString() == kullaniciId).ToList();
            string MenuKodlari = null;
            foreach (var item in vKullanici)
            {
                MenuKodlari = item.alt_menuler.alt_menu_adi +","+ MenuKodlari;
            }
            string[] strArrayOne = new string[] { "" };
            try { strArrayOne = MenuKodlari.Split(','); }
            catch { }
            return strArrayOne ;
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}