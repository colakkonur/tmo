﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using tmo.Models;
using tmo.Models.Login;

namespace tmo.Controllers
{
    public class LoginController : Controller
    {
        [HttpGet, Route("giris")]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost, Route("giris")]
        public ActionResult Login(kullanicilar gelen, loglar islem)
        {
            try
            {
                tmoEntities db = new tmoEntities();
                if (new LoginState().IsLoginSucces(gelen.kullanici_adi, gelen.parola))
                {
                    FormsAuthentication.SetAuthCookie(Convert.ToString(Session["kullanici_id"]), false);
                    islem.baslik = "Giriş yaptı.";
                    islem.islem_tarihi = DateTime.Now;
                    islem.kim_tarafindan = Convert.ToInt32(Session["kullanici_id"]);
                    db.loglar.Add(islem);
                    db.SaveChanges();
                    return RedirectToAction("Index", "Home");
                }
                TempData["message"] = "Kullanıcı adınız veya şifreniz yanlış!"; 
                return RedirectToAction("Login", "Login");
            }
            catch (Exception)
            {
                TempData["message"] = "Bir Hata Oluştu!";
                return RedirectToAction("Login", "Login");
            }
        }

        public ActionResult Logout()
        {
            Session.Clear();
            Response.Cookies[".ASPXAUTH"].Expires = DateTime.Now.AddDays(-1);
            return RedirectToAction("Index", "Home");
        }
    }
}