﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace tmo.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult PageError()
        {
            Response.TrySkipIisCustomErrors = true;
            return View();
        }
        public ActionResult Page404(string aspxerrorpath)
        {
            if (Session["kullanici_id"] == null)
            {
                return RedirectToAction("Login", "Login");
            }
            else
            {
                if (!string.IsNullOrEmpty(aspxerrorpath))
                {
                    Response.StatusCode = 404;
                    Response.TrySkipIisCustomErrors = true;
                    TempData["Hata"] =  "'" + aspxerrorpath + "'" + " Sayfası Bulunamadı.";
                    TempData["aciklama"] = "Aradığınız sayfa taşınmış, kaldırılmış, yeniden adlandırılmış veya hiç var olmamış olabilir.";
                }
                else
                {
                    TempData["Hata"] = "Bu Sayfaya Giriş Yetkiniz Bulunmamaktadır.";
                    TempData["aciklama"] = "Bu sayfaya girişiniz reddedildi. Lütfen yöneticiniz ile iletişime geçiniz.";
                }
                return View("PageError");
            }
        }
        public ActionResult Page403()
        {
            Response.StatusCode = 403;
            Response.TrySkipIisCustomErrors = true;
            return View("PageError");
        }
        public ActionResult Page500()
        {
            Response.StatusCode = 500;
            Response.TrySkipIisCustomErrors = true;
            return View("PageError");
        }
        
    }
}