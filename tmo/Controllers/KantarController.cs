﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tmo.Models;
using tmo.Models.Login;

namespace tmo.Controllers
{
    [ControlLogin]
    public class KantarController : Controller
    {
        // GET: Kantar
        [HttpGet]
        [Authorize(Roles = "MudiListesi")]
        public ActionResult MudiListesi()
        {
            tmoEntities db = new tmoEntities();
            var vMudiler = db.mudiler.ToList();
            var vIller = db.iller.ToList();
            var vIlceler = db.ilceler.ToList();
            var vAraciKurumlar = db.mudi_araci_kurumlar.ToList();
            return View(Tuple.Create(vMudiler, vIller, vIlceler, vAraciKurumlar));
        }

        [HttpGet]
        [Authorize(Roles = "MudiKayit")]
        public ActionResult MudiKayit()
        {
            tmoEntities db = new tmoEntities();
            var vMudiler = db.mudiler.ToList();
            var vIller = db.iller.ToList();
            var vIlceler = db.ilceler.ToList();
            var vAraciKurumlar = db.mudi_araci_kurumlar.ToList();
            return View(Tuple.Create(vMudiler, vIller, vIlceler, vAraciKurumlar));
        }

        [HttpPost]
        [Authorize(Roles = "MudiKayit")]
        public ActionResult MudiKayit(mudiler yeniMudi, loglar islem)
        {
            tmoEntities db = new tmoEntities();
            var vMudiler = db.mudiler.Where(w=>w.vn_tc.Equals(yeniMudi.vn_tc) == true).FirstOrDefault();
            if (vMudiler == null)
            {
                islem.baslik = "Mudi Kaydı Ekledi.";
                islem.islem_tarihi = DateTime.Now;
                islem.kim_tarafindan = Convert.ToInt32(Session["kullanici_id"]);
                yeniMudi.kayit_tarihi = DateTime.Now;
                db.loglar.Add(islem);
                db.mudiler.Add(yeniMudi);
                db.SaveChanges();
                TempData["approval"] = "Mudi Kaydı Başarılı.";
            }
            else { TempData["rejection"] = "Mudi Kaydı Daha Önce Yapılmış!"; }
            return RedirectToAction("MudiKayit");
        }

        [HttpPost]
        [Authorize(Roles = "MudiKayit")]
        public ActionResult MudiGuncelle(mudiler gelenMudi, loglar islem)
        {
            try
            {
                tmoEntities db = new tmoEntities();
                var vHangiMudi = db.mudiler.Where(w => w.mudi_id == gelenMudi.mudi_id).FirstOrDefault();

                vHangiMudi.ad_soyad_ünvan = gelenMudi.ad_soyad_ünvan;
                vHangiMudi.vn_tc = gelenMudi.vn_tc;
                vHangiMudi.vd = gelenMudi.vd;
                vHangiMudi.il_id = gelenMudi.il_id;
                vHangiMudi.ilce_id = gelenMudi.ilce_id;
                vHangiMudi.acik_adres = gelenMudi.acik_adres;
                vHangiMudi.araci_kurum_id = gelenMudi.araci_kurum_id;
                vHangiMudi.kayit_tipi = gelenMudi.kayit_tipi;
                vHangiMudi.guncellemeTarihi = DateTime.Now;

                islem.baslik = vHangiMudi.mudi_id.ToString() + "ID numaralı Mudi Kaydını Güncelledi.";
                islem.islem_tarihi = DateTime.Now;
                islem.kim_tarafindan = Convert.ToInt32(Session["kullanici_id"]);
                db.loglar.Add(islem);

                db.SaveChanges();
                TempData["approval"] = "Güncelleme Tamamlandı.";
                return RedirectToAction("MudiKayit");
            }
            catch (Exception)
            {
                TempData["rejection"] = "Bir Hata Oluştu.";
                return RedirectToAction("MudiKayit");
            }
        }


        [HttpGet]
        [Authorize(Roles = "TartimYap")]
        public ActionResult TartimYap()
        {
            tmoEntities db = new tmoEntities();
            var vTarimlar = db.tartim_bilgileri.ToList();
            return View(Tuple.Create(vTarimlar));
        }

        [HttpPost]
        [Authorize(Roles = "TartimYap")]
        public ActionResult TartimYap(tartim_bilgileri yeniTartim, loglar islem)
        {
            try
            {
                tmoEntities db = new tmoEntities();
                islem.baslik = "Yeni Tartım Yaptı.";
                islem.islem_tarihi = DateTime.Now;
                islem.kim_tarafindan = Convert.ToInt32(Session["kullanici_id"]);

                yeniTartim.kaydeden_personel_id = Convert.ToInt32(Session["kullanici_id"]);
                yeniTartim.kayit_tarihi = DateTime.Now;

                db.loglar.Add(islem);
                db.tartim_bilgileri.Add(yeniTartim);
                db.SaveChanges();
                TempData["approval"] = "Tartım Bilgileri Kaydedildi.";
                return RedirectToAction("TartimYap");
            }
            catch (Exception)
            {
                TempData["rejection"] = "Bir Hata Oluştu!";
                return RedirectToAction("TartimYap");
            }
        }

        [HttpPost]
        [Authorize(Roles = "TartimYap")]
        public ActionResult TartimGuncelle(tartim_bilgileri gelenTartim, loglar islem)
        {
            try
            {
                tmoEntities db = new tmoEntities();

                var vHangiTartim = db.tartim_bilgileri.Where(w => w.tartim_id == gelenTartim.tartim_id).FirstOrDefault();

                islem.baslik = gelenTartim.tartim_id + "ID numaralı Tartım Bilgisini Güncelledi.";
                islem.islem_tarihi = DateTime.Now;
                islem.kim_tarafindan = Convert.ToInt32(Session["kullanici_id"]);


                vHangiTartim.arac_cinsi = gelenTartim.arac_cinsi;
                vHangiTartim.ilk_tartim = gelenTartim.ilk_tartim;
                vHangiTartim.ikinci_tartim = gelenTartim.ikinci_tartim;
                vHangiTartim.net_miktar = gelenTartim.net_miktar;
                vHangiTartim.giris_mi_cikis_mi = gelenTartim.giris_mi_cikis_mi;
                vHangiTartim.degistiren_personel_id = Convert.ToInt32(Session["kullanici_id"]);
                gelenTartim.degisim_tarihi = DateTime.Now;

                db.loglar.Add(islem);
                db.SaveChanges();
                TempData["approval"] = "Tartım Bilgileri Güncellendi.";
                return RedirectToAction("TartimYap");
            }
            catch (Exception)
            {
                TempData["rejection"] = "Bir Hata Oluştu!";
                return RedirectToAction("TartimYap");
            }
        }
    }
}