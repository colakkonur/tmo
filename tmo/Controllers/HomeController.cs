﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tmo.Models;
using tmo.Models.Login;

namespace tmo.Controllers
{
    
    public class HomeController : Controller
    {
        [ControlLogin]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult altMenuler()
        {
            try
            {
                int iKullanici = Convert.ToInt32(Session["kullanici_id"]);
                tmoEntities db = new tmoEntities();
                var vHangiIzınler = db.kullanici_alt_menu.Where(w => w.kullanici_id == iKullanici).ToList();
                List<alt_menuler> Menuler = new List<alt_menuler>();
                foreach (var item in vHangiIzınler)
                {
                    var vHangiMenu = db.alt_menuler.Where(w => w.alt_menu_id == item.alt_menu_id).FirstOrDefault();
                    Menuler.Add(vHangiMenu);
                }
                return PartialView(Tuple.Create(Menuler));
            }
            catch (Exception)
            {
                return RedirectToAction("Login", "Login");
            }
        }
        
    }
}