﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tmo.Models;
using tmo.Models.Login;

namespace tmo.Controllers
{
    [ControlLogin]
    public class AdminController : Controller
    {
        // GET: Admin
        [HttpGet]
        [Authorize(Roles = "KullaniciEkle")]
        public ActionResult KullaniciEkle()
        {
            tmoEntities db = new tmoEntities();
            var vKullanicilar = db.kullanicilar.ToList();
            var vSubeler = db.subeler.ToList();
            var vAltmenuler = db.alt_menuler.ToList();
            return View(Tuple.Create(vKullanicilar, vSubeler, vAltmenuler));
        }

        [HttpPost]
        [Authorize(Roles = "KullaniciEkle")]
        public ActionResult KullaniciEkle(kullanicilar yeniKullanici)
        {
            try
            {
                tmoEntities db = new tmoEntities();
                var vKullaniciVarmi = db.kullanicilar.Where(w => w.kullanici_adi.Equals(yeniKullanici.kullanici_adi) == true).FirstOrDefault();
                if (vKullaniciVarmi == null)
                {
                    yeniKullanici.kayit_zamani = DateTime.Now;
                    db.kullanicilar.Add(yeniKullanici);
                    db.SaveChanges();
                    TempData["approval"] = "Kullanıcı Eklendi.";
                    return RedirectToAction("KullaniciEkle");
                }
                else
                {
                    TempData["rejection"] = "Kullanıcı Eklenemedi (Bu Kullanıcı Adı Kullanılıyor).";
                    return RedirectToAction("KullaniciEkle");
                }
            }
            catch (Exception)
            {
                TempData["rejection"] = "Kullanıcı Eklenirken bir hata oluştu.";
                return RedirectToAction("KullaniciEkle");
            }
        }

        [HttpPost]
        [Authorize(Roles = "KullaniciEkle")]
        public ActionResult KullaniciGuncelle(kullanicilar gelenKullanici)
        {
            try
            {
                tmoEntities db = new tmoEntities();
                var vKullaniciVarmi = db.kullanicilar.Where(w => w.kullanici_adi.Equals(gelenKullanici.kullanici_adi) == true && w.kullanici_id != gelenKullanici.kullanici_id).FirstOrDefault();
                if (vKullaniciVarmi == null)
                {
                    var vHangiKullanici = db.kullanicilar.Where(w => w.kullanici_id == gelenKullanici.kullanici_id).FirstOrDefault();
                    vHangiKullanici.ad_soyad = gelenKullanici.ad_soyad;
                    vHangiKullanici.sube_id = gelenKullanici.sube_id;
                    vHangiKullanici.rol = gelenKullanici.rol;
                    vHangiKullanici.kullanici_adi = gelenKullanici.kullanici_adi;
                    vHangiKullanici.parola = gelenKullanici.parola;
                    db.SaveChanges();
                    TempData["approval"] = "Kullanıcı Güncellendi.";
                    return RedirectToAction("KullaniciEkle");
                }
                else
                {
                    TempData["rejection"] = "Kullanıcı Güncellenemedi (Yeni Kullanıcı Adı Zaten Mevcut. Başka Bir Kullanıcı Adı Deneyiniz).";
                    return RedirectToAction("KullaniciEkle");
                }
            }
            catch (Exception)
            {
                TempData["rejection"] = "Kullanıcı Güncellenirken bir hata oluştu.";
                return RedirectToAction("KullaniciEkle");
            }
        }

        [HttpPost]
        [Authorize(Roles = "KullaniciEkle")]
        public ActionResult RolDuzenle(int kullanici_id, string MudiListesi, string MudiKayit, string TartimYap)
        {
            try
            {
                tmoEntities db = new tmoEntities();
                var vHangiKullanici = db.kullanicilar.Where(w => w.kullanici_id == kullanici_id).FirstOrDefault();
                var vHangiIzinler = db.kullanici_alt_menu.Where(w => w.kullanici_id == vHangiKullanici.kullanici_id).ToList();
                List<kullanici_alt_menu> izinler = new List<kullanici_alt_menu>();
                if (MudiListesi != null)
                {
                    var vMudiListesiIzni = db.kullanici_alt_menu.Where(w => w.kullanici_id == vHangiKullanici.kullanici_id && w.alt_menu_id == 1).FirstOrDefault();
                    if (vMudiListesiIzni == null)
                    {
                        kullanici_alt_menu yeniIzinler = new kullanici_alt_menu();
                        yeniIzinler.alt_menu_id = 1;
                        yeniIzinler.kullanici_id = vHangiKullanici.kullanici_id;
                        izinler.Add(yeniIzinler);
                    }
                }
                else
                {
                    var vMudiListesiIzni2 = db.kullanici_alt_menu.Where(w => w.kullanici_id == vHangiKullanici.kullanici_id && w.alt_menu_id == 1).FirstOrDefault();
                    if (vMudiListesiIzni2 != null)
                    {
                        db.kullanici_alt_menu.Remove(vMudiListesiIzni2);
                    }
                }

                if (MudiKayit != null)
                {
                    var vMudiKayitIzni = db.kullanici_alt_menu.Where(w => w.kullanici_id == vHangiKullanici.kullanici_id && w.alt_menu_id == 2).FirstOrDefault();
                    if (vMudiKayitIzni == null)
                    {
                        kullanici_alt_menu yeniIzinler = new kullanici_alt_menu();
                        yeniIzinler.alt_menu_id = 2;
                        yeniIzinler.kullanici_id = vHangiKullanici.kullanici_id;
                        izinler.Add(yeniIzinler);
                    }
                }
                else
                {
                    var vMudiListesiIzni2 = db.kullanici_alt_menu.Where(w => w.kullanici_id == vHangiKullanici.kullanici_id && w.alt_menu_id == 2).FirstOrDefault();
                    if (vMudiListesiIzni2 != null)
                    {
                        db.kullanici_alt_menu.Remove(vMudiListesiIzni2);
                    }
                }

                if (TartimYap != null)
                {
                    var vTartimYapIzni = db.kullanici_alt_menu.Where(w => w.kullanici_id == vHangiKullanici.kullanici_id && w.alt_menu_id == 4).FirstOrDefault();
                    if (vTartimYapIzni == null)
                    {
                        kullanici_alt_menu yeniIzinler = new kullanici_alt_menu();
                        yeniIzinler.alt_menu_id = 4;
                        yeniIzinler.kullanici_id = vHangiKullanici.kullanici_id;
                        izinler.Add(yeniIzinler);
                    }
                }
                else
                {
                    var vMudiListesiIzni2 = db.kullanici_alt_menu.Where(w => w.kullanici_id == vHangiKullanici.kullanici_id && w.alt_menu_id == 4).FirstOrDefault();
                    if (vMudiListesiIzni2 != null)
                    {
                        db.kullanici_alt_menu.Remove(vMudiListesiIzni2);
                    }
                }

                db.kullanici_alt_menu.AddRange(izinler);
                db.SaveChanges();

                return RedirectToAction("KullaniciEkle");
            }
            catch (Exception)
            {
                return RedirectToAction("KullaniciEkle");
            }
        }
    }
}